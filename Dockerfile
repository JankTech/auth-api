FROM php:8.1.3-fpm

RUN apt-get update -y
RUN docker-php-ext-install pdo_mysql
RUN apt-get install -y git
