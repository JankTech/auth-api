<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
	<meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

</head>
<body>
	<div id="app">
		<nav class="font-sans flex flex-col text-center content-center sm:flex-row sm:text-left sm:justify-between py-2 px-6 bg-white shadow sm:items-baseline w-full">
			<div class="mb-2 sm:mb-0 flex flex-row">
				<a class="h-10 w-10 self-center mr-2" href="{{ url('/') }}">
					<img class="h-10 w-10 self-center" src="{{ asset("img/janktech-logo.svg") }}"  alt="JankTech Logo"/>
				</a>
				<div>
					<a class="text-2xl no-underline text-grey-darkest hover:text-blue-600 font-sans font-bold" href="{{ url('/') }}">
						{{ config('app.name', 'Laravel') }}
					</a>
					<br/>
					<span class="text-xs text-grey-dark">It'll do</span>
				</div>
			</div>

			<div class="sm:mb-0 self-center">
				<!-- Authentication Links -->
				@guest
					@if (Route::has('login'))
						<a class="text-md no-underline text-grey-darker hover:text-blue-600 ml-2 px-1" href="{{ route('login') }}">{{ __('Login') }}</a>
					@endif

					@if (Route::has('register'))
						<a class="text-md no-underline text-black hover:text-blue-600 ml-2 px-1" href="{{ route('register') }}">{{ __('Register') }}</a>
					@endif
				@else
					<span class="text-md no-underline text-black ml-2 px-1">
						{{ Auth::user()->name }}
					</span>

					<a class="text-md no-underline text-grey-darker hover:text-blue-dark ml-2 px-1" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
						{{ __('Logout') }}
					</a>

					<form id="logout-form" action="{{ route('logout') }}" method="POST" class="hidden">
						@csrf
					</form>
				@endguest
			</div>
		</nav>
		<main class="py-4">
			@yield('content')
		</main>
	</div>
</body>
</html>
