<?php
declare(strict_types=1);

namespace App\Models\Auth;

use Illuminate\Support\Str;
use Laravel\Passport\Client as PassportClient;

class Client extends PassportClient
{
    /**
     * Determine if the client should skip the authorization prompt.
     *
     * @return bool
     */
    public function skipsAuthorization()
    {
        Str::random();
        return true;
    }
}
